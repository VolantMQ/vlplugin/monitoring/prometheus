module gitlab.com/VolantMQ/vlplugin/monitoring/prometheus

go 1.13

require (
	github.com/VolantMQ/vlapi v0.5.6
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v1.3.0
	gopkg.in/yaml.v3 v3.0.0-20200121175148-a6ecf24a6d71
)
